"""
Adapted from LINflow as a temporary fix for the error:
sh: sourmash: Argument list too long
"""

from sourmash.search import search_databases
import numpy as np
import pandas as pd
import glob
from os.path import join, isfile
import re

import sys
import os
from sourmash.logging import notify, error, set_quiet
from sourmash.index import LinearIndex
from sourmash import signature as sig
from sourmash.sbt import SBT
from sourmash.sbtmh import SigLeaf
from sourmash.lca import LCA_Database
from sourmash.sourmash_args import get_moltype, traverse_find_sigs, filter_compatible_signatures, \
	check_tree_is_compatible

base_dir = os.getenv("LINBASE_HOME").rstrip("/") + "/linproject/"
rep_bac_dir = base_dir + "workspace/sourmash2/rep_bac/"
sourmash_dir = base_dir + "workspace/sourmash2/all_sketches/"
DEFAULT_LOAD_K = 31
set_quiet(True)


def load_dbs_and_sigs(filenames, query, is_similarity_query, traverse=False):
	"""
	Load one or more SBTs, LCAs, and/or signatures.

	Check for compatibility with query.
	"""
	query_ksize = query.minhash.ksize
	query_moltype = get_moltype(query)

	n_signatures = 0
	n_databases = 0
	databases = []
	for sbt_or_sigfile in filenames:
		notify('loading from {}...', sbt_or_sigfile, end='\r')
		# are we collecting signatures from a directory/path?
		if traverse and os.path.isdir(sbt_or_sigfile):
			for sigfile in traverse_find_sigs([sbt_or_sigfile]):
				try:
					siglist = sig.load_signatures(sigfile,
												  ksize=query_ksize,
												  select_moltype=query_moltype)
					siglist = filter_compatible_signatures(query, siglist, 1)
					linear = LinearIndex(siglist, filename=sigfile)
					databases.append((linear, sbt_or_sigfile, False))
					notify('loaded {} signatures from {}', len(linear),
						   sigfile, end='\r')
					n_signatures += len(linear)
				except Exception:  # ignore errors with traverse
					pass

			# done! jump to beginning of main 'for' loop
			continue

		# no traverse? try loading as an SBT.
		try:
			tree = SBT.load(sbt_or_sigfile, leaf_loader=SigLeaf.load)

			if not check_tree_is_compatible(sbt_or_sigfile, tree, query,
											is_similarity_query):
				sys.exit(-1)

			databases.append((tree, sbt_or_sigfile, 'SBT'))
			notify('loaded SBT {}', sbt_or_sigfile, end='\r')
			n_databases += 1

			# done! jump to beginning of main 'for' loop
			continue
		except (ValueError, EnvironmentError):
			# not an SBT - try as an LCA
			pass

		# ok. try loading as an LCA.
		try:
			lca_db = LCA_Database.load(sbt_or_sigfile)

			assert query_ksize == lca_db.ksize
			query_scaled = query.minhash.scaled

			notify('loaded LCA {}', sbt_or_sigfile, end='\r')
			n_databases += 1

			databases.append((lca_db, sbt_or_sigfile, 'LCA'))

			continue
		except (ValueError, TypeError, EnvironmentError):
			# not an LCA database - try as a .sig
			pass

		# not a tree? try loading as a signature.
		try:
			siglist = sig.load_signatures(sbt_or_sigfile,
										  ksize=query_ksize,
										  select_moltype=query_moltype)
			siglist = list(siglist)
			if len(siglist) == 0:  # file not found, or parse error?
				raise ValueError

			siglist = filter_compatible_signatures(query, siglist, False)
			linear = LinearIndex(siglist, filename=sbt_or_sigfile)
			databases.append((linear, sbt_or_sigfile, 'signature'))

			notify('loaded {} signatures from {}', len(linear),
				   sbt_or_sigfile, end='\r')
			n_signatures += len(linear)
		except (EnvironmentError, ValueError) as e:
			raise e

	if n_signatures and n_databases:
		notify('loaded {} signatures and {} databases total.', n_signatures,
			   n_databases)
	elif n_signatures:
		notify('loaded {} signatures.', n_signatures)
	elif n_databases:
		notify('loaded {} databases.', n_databases)
	else:
		sys.exit(-1)

	if databases:
		print('')

	return databases


def compare_sketch(query_sigs_path, lingroup, k, threshold=0.0001) -> pd.DataFrame:
	"""
	Compare signatures using sourmash

	Parameters
	----------
	query_sigs : List of SourmashSignature
		list of sourmash signatures
	lingroup : str
		determined LINgroup membership to save under the result under the appropriate path
	k : int
		window size for comparing signatures
	threshold: float
		search threshold for sourmash

	Returns
	-------
	 df_list  : pandas DataFrame
		Sourmash result format
		File header [similarity,name,filename,md5] e.g. 0.xxx,/path/to/query/zzz.fasta,
		path/to/workspace/Signatures/rep_back/1.sig,md5 of query file
	"""
	if lingroup == "rep_bac":
		dest = rep_bac_dir
	else:
		dest = join(sourmash_dir, lingroup)
	folder_size = len([file for file in os.listdir(dest) if isfile(join(dest, file))])
	if type(k) == str:
		k = int(k)

	query_sigs = sig.load_signatures(query_sigs_path, ksize=k)
	query_sigs = list(query_sigs)
	if len(query_sigs) == 0:  # file not found, or parse error?
		raise ValueError

	# get the appropriate k-mer signature
	query = None
	for signature in query_sigs:
		if signature.minhash.ksize == int(k):
			query = signature

	all_sigs = glob.glob(dest + "/*.sig")
	registered_sigs = [file for file in all_sigs if re.match(r'.*\/[0-9]+.sig$', file)]
	new_sigs = np.setdiff1d(np.array(all_sigs), np.array(registered_sigs)).tolist()
	while True:
		try:
			ref_signatures = load_dbs_and_sigs(registered_sigs + new_sigs, query,
											   is_similarity_query=False, traverse=False)
		# this is thrown when a file in the list is removed from the directory during loading
		except (EnvironmentError, ValueError) as e:
			continue
		break

	results = search_databases(query, ref_signatures, threshold=threshold,
							   do_containment=False, best_only=False, ignore_abundance=False)
	# cmd = "sourmash search {0} {1} -n {2} -k {4} -q --threshold 0.0001 -o {3}"
	# cmd = cmd.format(query, join(dest, '*.sig'), folder_size, join(sourmash_result, "tmp_result.txt"), k)

	# Organize results into a dataframe
	df_list = []
	index = []
	for result in results:
		record = dict()

		record['similarity'] = result.similarity
		record['name'] = result.name
		record['filename'] = result.filename
		record['md5'] = result.md5
		# should an int be appended?
		index.append(str(os.path.splitext(os.path.basename(result.filename))[0]))
		df_list.append(record)
	return pd.DataFrame(df_list, index)
