#!/usr/bin/env python3
"""
This is intended to be used to update metadata for genomes already in the database
used dynamic as search key (see first line of main)
"""

import os
import sys
import numpy as np
import pandas as pd
import re
from LINdb import DB_NAME, LINdb
from workflow2 import get_Tax_ID_by_entry

UPDATE_METADATA_QUERY = "UPDATE AttributeValue JOIN Attribute USING (Attribute_ID) " \
						"SET AttributeValue = {0} " \
						"WHERE Genome_ID = {1} AND AttributeName = {2};"
UPDATE_TAXONOMY_QUERY = "UPDATE Taxonomy JOIN Taxonomic_ranks USING (Rank_ID) " \
						"SET Taxon = {0}, NCBI_Tax_ID = {1} " \
						"WHERE Genome_ID = {2} AND Rank_ID = {3};"

UPDATE_METADATA_QUERY2 = "INSERT INTO AttributeValue (Genome_ID, Interest_ID, Attribute_ID, AttributeValue, User_ID) VALUES " \
						"({0},{1},{2},{3},{4}) ON DUPLICATE KEY UPDATE AttributeValue=VALUES(AttributeValue), User_ID=VALUES(User_ID);"

UPDATE_TAXONOMY_QUERY2 = "INSERT INTO Taxonomy (Genome_ID, Rank_ID, NCBI_Tax_ID, Taxon) VALUES " \
						"({0},{1},{2},{3}) ON DUPLICATE KEY UPDATE NCBI_Tax_ID=VALUES(NCBI_Tax_ID), Taxon=VALUES(Taxon);"


def sql_sting_wrapper(val: str) -> str:
	if pd.isna(val):
		return "NULL"
	elif f"{val}".isnumeric() or type(val) is float:
		return val
	elif val and val != "":
		val = val.strip('"\'')
		return '"' + val + '"'
	else:
		return "NULL"


def update_scaffold_count(db: LINdb, gid: int):
	query = UPDATE_METADATA_QUERY
	filepath = db.get_genome_path(gid)
	col = "Number of contigs"
	val = 0
	if not os.path.isfile(filepath):
		print("Scaffold count failed. File not found {0}".format(filepath))
	with open(filepath, "r") as fasta:
		line = fasta.readline()
		while line:
			if line.startswith(">"):
				val += 1
			line = fasta.readline()

	db.query_first(query.format(val, gid, sql_sting_wrapper(col)))


def get_rank_dict() -> dict:
	query = "SELECT `Rank`, Rank_ID FROM Taxonomic_ranks"
	result = db.query_all(query)
	ranks = {}
	for val in result:
		rank, id = val
		ranks[rank] = id
	return ranks

def get_attribute_dict() -> dict:
	query = "SELECT AttributeName, Attribute_ID FROM Attribute"
	result = db.query_all(query)
	ranks = {}
	for val in result:
		rank, id = val
		ranks[rank] = id
	return ranks


def find_taxid(db: LINdb, term: str, rank: int):
	query = "SELECT NCBI_Tax_ID FROM NCBI_Tax_ID WHERE Taxon= {0} AND Rank_ID= {1}"
	result = db.query_first(query.format(sql_sting_wrapper(term), rank))
	if result and len(result) > 0 and str(result[0]).isnumeric():
		return int(result[0])
	else:
		return None


def get_species_taxid(db: LINdb, species: str, genus=None, species_rank=7):

	singleton = find_taxid(db, species, species_rank)
	if singleton:
		return singleton
	if not genus or genus == "":
		return None
	combined_species = f"{genus} {species}"
	combined = find_taxid(db, combined_species, species_rank)
	return combined


def getattributeID(name):
	query = f"Select Attribute_ID from Attribute where AttributeName = {name}"
	try:
		return db.query_first(query)
	except:
		return None

if __name__ == '__main__':
	UPDATE_KEY = "Genome_ID"
	metadata_file = sys.argv[1]
	df = pd.read_csv(metadata_file, header=0)
	DB_NAME = sys.argv[2] if len(sys.argv) > 2 else DB_NAME
	db = LINdb(database_name=DB_NAME)
	columns = list(df.columns)
	columns.remove(UPDATE_KEY)
	try: columns.remove("Download Path")
	except: pass
	ranks = get_rank_dict()

	attribute_dict = get_attribute_dict()
	if UPDATE_KEY in attribute_dict.keys():
		search_query = "SELECT Genome_ID FROM Genome JOIN AttributeValue USING (Genome_ID) WHERE Attribute_ID = {0} " \
				"AND AttributeValue = {1};"
	else:
		search_query = "SELECT Genome_ID FROM Genome WHERE {0} = {1};"
		attribute_id = UPDATE_KEY

	for index, row in df.iterrows():
		identifier = row[UPDATE_KEY]
		# get Genome_ID based on Accession
		result = None
		result = db.query_all(search_query.format(attribute_id, sql_sting_wrapper(identifier)))
		if len(result) != 1:
			print("Too many or too few results ({0}) for identifier {1}: {2}".format(
				len(result), identifier, list(result)
			))
			continue
		gid = int(result[0][0])
		interest = 1
		if "interest" in columns:
			interest = row["interest"]
		#update_scaffold_count(db, gid)
		# Update all columns in the CSV for each Genome_ID
		for col in columns:
			val = row[col]
			
			if col in ranks.keys():
				rank = ranks[col]
				# search for the combine species with genus as well when looking for species taxid
				if col == "species" and row["genus"]:
					taxid = get_species_taxid(db, val, row["genus"], rank)
				else:
					taxid = find_taxid(db, val, rank)
				# if a taxid could not be found use val otherwise only keep taxid
				if not taxid:
					taxid = 0
				else:
					val = None
				query = UPDATE_TAXONOMY_QUERY2.format(gid, rank, taxid, sql_sting_wrapper(val))
			else:
				if pd.isna(val):
					continue
				query = UPDATE_METADATA_QUERY2.format(gid, interest, attribute_dict[col], sql_sting_wrapper(val), 71)
			db.query_first(query)
			db.commit()

		print(f"Updated metadata for genome with {UPDATE_KEY}: {gid}")