#!/usr/bin/python

'''
	Login file should be in the following format (don't forget to make it readable only by you)
	[client]
	username=yourusername
	password=yourpassowrd
'''

import sys
import MySQLdb
from MySQLdb import Connect
from hash_genome import hash_genome

host = "127.0.0.1"
login_file = "~/.my.cnf"
DB_NAME = "LINbase"

class LINdb(object):
	"""
	Object to interact with the MySQL database
	"""

	def __init__(self, database_name=DB_NAME):
		self.conn, self.cursor = connect_to_db()
		self.db_name = database_name
		if database_exists(database_name):
			self.cursor.execute("USE `{}`".format(self.db_name))

	def get_cursor(self):
		return self.cursor

	def commit(self):
		self.conn.commit()

	def initialize(self):
		'''
		if database_exists(self.db_name):
			print("Database with the name {} exists please delete the database"
				  " or select a new name".format(self.db_name))

		self.cursor.execute("CREATE DATABASE `{}`".format(self.db_name))
		'''

	def is_empty(self):
		self.cursor.execute("SELECT COUNT(*) FROM Genome")
		rows = self.cursor.fetchone()[0]
		return False if rows > 0 else True

	# obj is of type getLIN
	def add_lin_async(self, obj, genome_id):
		# genome_id, scheme_id, num_lins, conserved_lin
		tail_length = obj.label_num - len(obj.conserved_LIN.split(','))

		# if there is a tail
		if obj.idx_to_change == -1:
			self.add_lin(genome_id, obj.Scheme_ID, obj.conserved_LIN)
			return
		elif obj.idx_to_change == 0:
			head = ''
			tail = "".join([',0'] * (obj.label_num - 1))
			wildcard = "%" + "".join([',0'] * (obj.label_num - 1))
		else:
			tail = "".join([',0'] * (tail_length - 1))
			wildcard = '%'
			head = obj.conserved_LIN + ","

		query = "INSERT INTO LIN (Genome_ID,Scheme_ID,LIN) " \
				"SELECT {0}, {1}, CONCAT(\"{2}\", " \
				"MAX(SUBSTRING_INDEX(SUBSTRING_INDEX(LIN,',',{3}),',',1)) + 1, \"{4}\") " \
				"FROM LIN WHERE Scheme_ID={1} AND LIN LIKE \"{2}{5}\"".format(
			genome_id, obj.Scheme_ID, head, -1 * tail_length, tail, wildcard)

		try:
			self.cursor.execute(query)
		except Exception as e:
			print("SQL ERROR: " + query + "\n")
			print(e)

	def add_genome(self, filepath: str) -> int:
		self.cursor.execute("INSERT INTO Genome (FilePath, Hash) VALUES ('{0}', '{1}')".format(
			filepath, hash_genome(filepath)))
		return self.cursor.lastrowid

	def add_scheme(self, LIN_percents: str, number_of_labels: int, description="") -> int:
		self.cursor.execute(
			"INSERT INTO Scheme (Cutoff, LabelNum, Description) VALUES "
			"('{0}', {1}, '{2}')".format(LIN_percents, number_of_labels, description)
		)
		return self.cursor.lastrowid

	def add_lin(self, genome_id: int, scheme_id: int, lin: str):
		self.cursor.execute(
			"INSERT INTO LIN (Genome_ID,Scheme_ID,LIN) VALUES ({0},{1},'{2}')".format(
				genome_id, scheme_id, lin)
		)

	def add_duplicate(self, genome_id: int, userID: int):
		self.cursor.execute(
			"INSERT INTO Duplicated_upload (Reference_Genome_ID,Who_uploads_too) VALUES ({0},{1})".format(
				genome_id, userID)
		)


	# def add_taxonomy(self, genome_id, strain, genus='', species=''):

	def get_genome_path(self, genome_id: int) -> str:
		self.cursor.execute(
			"SELECT FilePath FROM Genome WHERE Genome_ID = {0}".format(genome_id)
		)
		results = self.cursor.fetchone()
		if results is not None and len(results) > 0:
			return results[0]
		return None

	def get_genomeID(self, filename: str):
		self.cursor.execute(
			"SELECT Genome_ID FROM Genome WHERE FilePath like '{0}'".format(filename)
		)
		results = self.cursor.fetchone()
		if results is not None and len(results) > 0:
			return int(results[0])
		return None

	def get_lin(self, genome_id, scheme_id=4):
		self.cursor.execute(
			"SELECT LIN FROM LIN WHERE Genome_ID={0} AND Scheme_ID={1}".format(genome_id, scheme_id)
		)
		results = self.cursor.fetchone()
		if results is not None and len(results) > 0:
			return results[0]
		return None

	def get_userID(self, username: str):
		self.cursor.execute(
			"SELECT User_ID FROM User WHERE Username='{0}'".format(username)
		)
		results = self.cursor.fetchone()
		if results is not None and len(results) > 0:
			return int(results[0])
		return None

	def get_interestID(self, name: str):
		self.cursor.execute(
			"SELECT Interest_ID FROM Interest WHERE InterestName='{0}'".format(name)
		)
		results = self.cursor.fetchone()
		if results is not None and len(results) > 0:
			return int(results[0])
		return None

	def get_duplicate_genome(self, hash: str):
		results = self.cursor.execute("SELECT Genome_ID from Genome where Hash='{0}' ".format(hash))
		genome = self.cursor.fetchone()
		return genome[0] if genome is not None and len(genome) > 0 else None

	def len_scheme(self, scheme_id: int) -> int:
		self.cursor.execute(
			"SELECT LabelNum FROM Scheme WHERE SCHEME_ID={0}".format(scheme_id)
		)
		results = self.cursor.fetchone()
		if results is not None and len(results) > 0:
			return int(results[0])
		return None

	def hide_genomes(self, genome_list: list):
		self.cursor.execute(
			"UPDATE LIN_back SET Visible=0 WHERE Genome_ID IN {0}".format(tuple(genome_list) + (-1, -2,))
		)
		self.commit()

	def query_first(self, query: str):
		self.cursor.execute(query)
		return self.cursor.fetchone()

	def query_all(self, query: str):
		self.cursor.execute(query)
		return self.cursor.fetchall()

	def close(self):
		self.conn.close()


def database_exists(db_name) -> bool:
	conn, cursor = connect_to_db()
	cursor.execute("SELECT SCHEMA_NAME FROM information_schema.schemata "
				   "where SCHEMA_NAME =  '{}'".format(db_name))
	db_exists = cursor.fetchall()
	if len(db_exists) > 0:
		return True
	return False


def getDB():
	db = Connect(host=host, read_default_file=login_file)
	query = db.cursor()

	return db, query


def connect_to_db():
	conn = Connect(host=host, read_default_file=login_file)
	c = conn.cursor()
	c.execute(f"use {DB_NAME};")

	return conn, c

