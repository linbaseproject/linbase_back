import hashlib
BLOCKSIZE = 65536


def hash_genome(filepath):
	hasher = hashlib.sha256()
	with open(filepath, 'rb') as file:
		buf = file.read(BLOCKSIZE)
		while len(buf) > 0:
			hasher.update(buf)
			buf = file.read(BLOCKSIZE)
	return hasher.hexdigest()
